/*Adding an array for storing all information about every pizza on the menu. Structure: 0 = Number and type, 1 = image link, 2 = Description, 3 = Allergens, 4 = price */

const menu_item_information = [
    ['1. Margherita', 'img/margherita.jpg', 'Pizza with mozzarella cheese and tomato sauce', 'Allergens: lactose, gluten', 199], 
    ['2. Hawaii', 'img/hawaii.jpg', 'Pizza with mozzarella cheese, tomato sauce, ham and pineapple', 'Allergens: lactose, gluten', 219], 
    ['3. Diavola', 'img/the_pizza.jpg', 'Pizza with mozzarella cheese, tomato sauce, chili, mushroom and olives', 'Allergens: lactose, gluten', 229], 
    ['4. Pepperoni', 'img/pepperoni.jpg', 'Pizza with mozzarella cheese, tomato sauce, pepperoni, onion and basil', 'Allergens: lactose, gluten', 229], 
    ['5. Ham & Cheese', 'img/ham_and_cheese.jpg', 'Pizza with mozzarella cheese, tomato sauce and ham', 'Allergens: lactose, gluten', 199], 
    ['6. Double meat', 'img/double_meat.jpg', 'Pizza with mozzarella cheese, tomato sauce, beef, pepperoni and ham', 'Allergens: lactose, gluten', 229], 
    ['7. The bay', 'img/the_bay.jpg', 'Pizza with mac & cheese', 'Allergens: lactose, gluten', 229], 
    ['8. Picante', 'img/picante.jpg', 'Pizza with white sauce and bacon', 'Allergens: lactose, gluten', 219], 
    ['9. Classico', 'img/classico.jpg', 'Classic italian pizza with mozzarella and basil.', 'Allergens: lactose, gluten', 229], 
    ['10. Chicken special', 'img/chicken_special.jpg', 'Pizza with buffalo chicken, tomato sauce and garlic sauce', 'Allergens: lactose, gluten, garlic', 229]];


/* Variables for keeping track of the pizzas the user has added to the cart. The current pizza the user is looking at, this is updated when the user moves to big pizza card. The cart state, it keeps track of if the cart har been redered in HTML yet or not. */

let pizzas_in_cart = [];
let current_pizza = 0;
let cart_state = 0;


/* Function for moving from the small pizza cards in the menu to the bigger pizza card with more information */

function move_to_big_pizza_card (self_info) {
    const pizza_id = parseInt(self_info.id);
    const menu_boxes = document.getElementById('menu_box');
    menu_boxes.style.display = 'none';
    const pizzacard = document.querySelector('.big_pizza_card');
    pizzacard.style.display = 'flex';
    console.log(pizza_id)
    document.getElementById('big_pizza_card_img').setAttribute('src', menu_item_information[pizza_id][1]);
    document.getElementById('big_pizza_card_heading').innerHTML = menu_item_information[pizza_id][0];
    document.getElementById('big_pizza_card_price').innerHTML = menu_item_information[pizza_id][4];
    document.getElementById('big_pizza_card_description').innerHTML = menu_item_information[pizza_id][2];
    document.getElementById('big_pizza_card_allergens').innerHTML = menu_item_information[pizza_id][3];
    current_pizza = pizza_id;
}

/* Function for adding items to the cart array and updating the cart button with current amount of items in the cart. */

function add_to_cart () {
    pizzas_in_cart.push(current_pizza);
    document.getElementById('cart_navbar_button').style.display = 'block';
    document.getElementById('cart_dot').style.display = 'block';
    document.getElementById('cart_navbar_button').innerHTML = 'CART ' + '(' + pizzas_in_cart.length + ')';
    document.getElementById('cart_hamburger_button').style.display = 'block';
    document.getElementById('cart_hamburger_button').innerHTML = 'CART ' + '(' + pizzas_in_cart.length + ')';
    cart_state = 0;
}

/* Function for moving to the cart via order now button in the big pizza card or cart button in navbar. Based on the current cart state it either calls move to cart to render the cart, or just displays the already rendered cart. */

function go_to_cart() {
    if (cart_state === 1) {
        const pizzacard = document.querySelector('.big_pizza_card');
        pizzacard.style.display = 'none';
        const menu_boxes = document.getElementById('menu_box');
        menu_boxes.style.display = 'none';
        const order_form = document.querySelector('.orderpage');
        order_form.style.display = 'flex';
        cart_state = 1;
    }
    else {
        move_to_cart();
        cart_state = 1;
    }
}

/* move_to_cart is only called by go_to_cart and it renders the cart based on how many of each pizza you have in the cart. It injects html for each uniqe type of pizza in the cart and updates with correct picture, price and number of each item.  */

function move_to_cart () {
    cart_state = 1;
    document.getElementById('order_info_section').innerHTML = '';
    const pizzacard = document.querySelector('.big_pizza_card');
    pizzacard.style.display = 'none';
    const menu_boxes = document.getElementById('menu_box');
    menu_boxes.style.display = 'none';
    const order_form = document.querySelector('.orderpage');
    order_form.style.display = 'flex';
    const unique_pizzas = [];
    result = { };
    for(let i = 0; i < pizzas_in_cart.length; ++i) {
        if(!result[pizzas_in_cart[i]])
            result[pizzas_in_cart[i]] = 0;
        ++result[pizzas_in_cart[i]];
    }
    for (let i = 0; i < pizzas_in_cart.length; i++) {
        if (!(unique_pizzas.includes(pizzas_in_cart[i]))) {
            unique_pizzas.push(pizzas_in_cart[i]);
            const htmlTemplate = '<div id="'+ pizzas_in_cart[i] + "c" +'" class="order_info flex_row_center_center margin_auto"><div class="order_info_image"><img class="order_info_image" src="' + menu_item_information[pizzas_in_cart[i]][1] + '"></div><div class="right_order_info_card"><div><p class="selected_pizza_in_cart">' + menu_item_information[pizzas_in_cart[i]][0] + '</p></div><div class="add_or_remove"><button id="'+ pizzas_in_cart[i] +'" onclick="remove_pizza(this)" class="button">-</button><button id="'+ pizzas_in_cart[i] +'" onclick="add_pizza(this)" class="button">+</button><p id="'+ pizzas_in_cart[i] + "b" +'" class="number_of_selected_pizza">' + result[pizzas_in_cart[i]] + '</p></div><div><p id="'+ pizzas_in_cart[i] + "a" +'" class="price_of_pizza_in_cart">' + menu_item_information[pizzas_in_cart[i]][4] * result[pizzas_in_cart[i]] + ' kr' + '</p></div></div></div>';
            document.getElementById('order_info_section').innerHTML += htmlTemplate;
        } 
    }
    calculate_total()
}

/* update_cart_items updates number of each pizza the user has in the cart. The user can add or remove pizzas from the cart in the orderpage. This needs to update the array, number on the cart button in the navbar and calcuate new price and total price of all items in the cart.*/
function update_cart_items (id) {
    result = { };
    for(let i = 0; i < pizzas_in_cart.length; ++i) {
        if(!result[pizzas_in_cart[i]])
            result[pizzas_in_cart[i]] = 0;
        ++result[pizzas_in_cart[i]];
    }
    if (result[id] == undefined) {
        document.getElementById(id + 'c').style.display = 'none';
        if (pizzas_in_cart.length == 0) {
            document.getElementById('cart_navbar_button').style.display = 'none';
            document.getElementById('cart_hamburger_button').style.display = 'none';
            document.getElementById('cart_dot').style.display = 'none';
        } 
        else {
            document.getElementById('cart_navbar_button').innerHTML = 'CART ' + '(' + pizzas_in_cart.length + ')';
            document.getElementById('cart_hamburger_button').innerHTML = 'CART ' + '(' + pizzas_in_cart.length + ')';
        }
    }
    else {
        document.getElementById(id + "b").innerHTML = result[id];
        document.getElementById(id + "a").innerHTML = menu_item_information[id][4] * result[id] + ' kr';
        document.getElementById('cart_navbar_button').innerHTML = 'CART ' + '(' + pizzas_in_cart.length + ')';
        document.getElementById('cart_hamburger_button').innerHTML = 'CART ' + '(' + pizzas_in_cart.length + ')';
    }
}

/* calculate_total calculates the total price of all the pizzas the user has in the cart and updates the correct html element.*/

function calculate_total() {
    let total_cost = 0
    for (let i = 0; i < pizzas_in_cart.length; i++) {
       const price = menu_item_information[pizzas_in_cart[i]][4];
       total_cost += price;
       document.getElementById("totalcost").innerHTML = "Total: " +  total_cost + "kr";
    }
    return total_cost;
}

/* add_pizza adds more of one item to the cart and calls update_cart_items to update everything that needs to be updated. */
function add_pizza(self) {
    pizzas_in_cart.push(parseInt(self.id)); 
    update_cart_items(self.id);
    calculate_total();
}

/* remove_pizza removes one item in the cart and calls update_cart_items to update everything that needs to be updated. */

function remove_pizza(self) {
    const index = pizzas_in_cart.indexOf(parseInt(self.id));
    if (index > -1) {
        pizzas_in_cart.splice(index, 1);
    }
    update_cart_items(self.id);
    if (pizzas_in_cart.length == 0) {
        back_to_menu()
    }
    calculate_total();
}

/* back_to_menu moves the user back to the menu while keeping the current cart. If the user adds more to the cart while the cart is already rendered the update cart items needs to be called for the cart to be rendered properly. */

function back_to_menu() {
    const pizzacard = document.querySelector('.big_pizza_card');
    pizzacard.style.display = 'none';
    const order_form = document.querySelector('.orderpage');
    order_form.style.display = 'none';
    const order_confirmation = document.getElementById('order_confirmation');
    order_confirmation.style.display = 'none';
    const menu_boxes = document.getElementById('menu_box');
    menu_boxes.style.display = 'flex';
}




/* Function to remove the big pizza card and "revert" back to menu items if the user want to check out another pizza. */

function remove_big_pizza_card() {
    const menu_boxes = document.getElementById('menu_box');
    menu_boxes.style.display = 'flex';
    const pizzacard = document.querySelector('.big_pizza_card');
    pizzacard.style.display = 'none';
}


/* move_to_order_confirmation takes all the information the user has provided in the orderpage, aswell as the items they have ordered. It removes the cart and renderes a order confirmation. This ensures that the user does not have to refresh the page to order another pizza. */

function move_to_order_confirmation() { 
    window.scrollTo(0, 0);
    let name = document.getElementById('order_name').value;
    let first_name = name.split(" ");
    let address = document.getElementById('order_address').value;
    let delivery = document.getElementById('input_delivery').value;
    const pizzacard = document.querySelector('.big_pizza_card');
    pizzacard.style.display = 'none';
    const order_form = document.querySelector('.orderpage');
    order_form.style.display = 'none';
    const menu_boxes = document.getElementById('menu_box');
    menu_boxes.style.display = 'none';
    const order_confirmation = document.getElementById('order_confirmation');
    order_confirmation.style.display = 'flex';
    document.getElementById('thanks_name').innerHTML = 'Thank you for your order ' + first_name[0] + '!';
    const order_details_name = document.getElementById('order_details_name').innerHTML = name;
    document.getElementById('order_details_phone_number').innerHTML = document.getElementById('phone_number').value;
    document.getElementById('order_details_email').innerHTML = document.getElementById('email').value;
    document.getElementById('order_details_address').innerHTML = document.getElementById('order_address').value + " " + document.getElementById('post_code').value + " " + document.getElementById('city').value;
    document.getElementById('order_details_time').innerHTML = document.getElementById('time_input').value;
    document.getElementById('order_details_date').innerHTML = document.getElementById('date_input').value;
    document.getElementById('order_details_delivery_method').innerHTML = document.getElementById('input_delivery').value;
    if (delivery == "pickup") {
        document.getElementById('delivery_text').innerHTML = "Your order is now being made! <br> Estimated time to pickup is " + Math.floor((pizzas_in_cart.length * 15) / 2) + ' minutes';
    }
    else {
        document.getElementById('delivery_text').innerHTML = "Your order is now being made! <br> Estimated time to deliver at " + address + " is " + Math.floor((pizzas_in_cart.length * 30) / 2) + " minutes";
    }
    document.getElementById('ordered_items').innerHTML = ""
    const unique_pizzas = [];
    result = { };
    for(let i = 0; i < pizzas_in_cart.length; ++i) {
        if(!result[pizzas_in_cart[i]])
            result[pizzas_in_cart[i]] = 0;
        ++result[pizzas_in_cart[i]];
    }
    for (let i = 0; i < pizzas_in_cart.length; i++) {
        if (!(unique_pizzas.includes(pizzas_in_cart[i]))) {
            unique_pizzas.push(pizzas_in_cart[i]);
            const htmlTemplate = '<div id="'+ pizzas_in_cart[i] + "c" +'" class="order_details_info flex_row_center_center margin_auto"><div class="order_info_image"><img class="order_info_image" src="' + menu_item_information[pizzas_in_cart[i]][1] + '"></div><div class="right_order_info_card"><div><p class="selected_pizza_in_cart">' + menu_item_information[pizzas_in_cart[i]][0] + '</p></div><div class="add_or_remove"><p id="'+ pizzas_in_cart[i] + "b" +'" class="number_of_selected_pizza">' + result[pizzas_in_cart[i]] + '</p></div><div><p id="'+ pizzas_in_cart[i] + "a" +'" class="price_of_pizza_in_cart">' + menu_item_information[pizzas_in_cart[i]][4] * result[pizzas_in_cart[i]] + ' kr' + '</p></div></div></div>';
            document.getElementById('ordered_items').innerHTML += htmlTemplate;
        } 
    }
    document.getElementById('order_total').innerHTML = "Order total: " + calculate_total();
    pizzas_in_cart = [];
    document.getElementById('cart_navbar_button').style.display = 'none'
    document.getElementById('cart_hamburger_button').style.display = 'none'

}



/* JS to add a "now" functionality to the time selector on the order form, this can also be used in the booking page. Just add a button with id="now_button" and have the time imput element with id="time_input". */ 

document.getElementById("now_button").addEventListener('click', (event) => {
    event.preventDefault();
    const today = new Date();
    let timeNow = "";
    /* Due to how the getMinutes function returns the number of minutes, in the time space 00 to 09 adding a 0 is neccacary to make the html work correctly */
    if (today.getMinutes() < 10) {
        timeNow = today.getHours() + ":" + "0" + today.getMinutes();
    } else {
        timeNow = today.getHours() + ":" + today.getMinutes();
    };
    const time_input = document.getElementById('time_input');
    console.log(today.getMinutes());
    time_input.setAttribute('value', timeNow);
})
