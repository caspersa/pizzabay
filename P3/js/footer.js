//Writes the html for the footer
document.write(
    '<div class="flex_row_center_center white_background footer">\
<div class="flex_column_center_center">\
    <p class="margin_zero">Opening hours:</p>\
    <p class="margin_zero">Every day</p>\
    <p class="margin_zero">10:00 - 23:00</p>\
</div>\
<div class="flex_column_center_center">\
    <p class="margin_zero">Visit us:</p>\
    <p class="margin_zero">Olav Trygvassonsgate 1</p>\
</div>\
<div class="flex_column_center_center">\
    <p class="margin_zero">Email:</p>\
    <p class="margin_zero">pizzabay@email.com</p>\
</div>\
<div class="flex_column_center_center">\
    <p class="margin_zero">Call us:</p>\
    <p class="margin_zero">+47 555 555 55</p>\
</div>\
</div>\
'
);
