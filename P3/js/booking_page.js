const btn = document.getElementById("booking_button");
const Booking_div = document.getElementById("booking_div");
const Confirmation = document.getElementById("Confirmation");
document.forms["Booking_form"].addEventListener("submit", function (event) {
    let fname = document.getElementById("fname").value;
    let email = document.getElementById("email").value;
    let phone = document.getElementById("phone").value;
    let date = document.getElementById("date").value;
    let time = document.getElementById("time_input").value;
    let number = document.getElementById("number").value;
    let allergies = document.getElementById("allergies").value;
    // If email, phone number or allergies are not filled out, a default value is set
    if (email == "") {
        email = "No e-mail";
    }
    if (phone == "") {
        phone = "No phone number";
    }
    if (allergies == "") {
        allergies = "No allergies";
    }
    // Hides Booking_div and displays Confirmation
    Booking_div.style.display = "none";
    Confirmation.style.display = "block";
    const fname_msg = "Full name: " + fname + "<br>";
    const email_msg = "E-mail: " + email + "<br>";
    const phone_msg = "Phone number: " + phone + "<br>";
    const req_msg =
        "You have reserved a table for " +
        number +
        " on " +
        date +
        " at " +
        time +
        "<br> Allergies: " +
        allergies +
        "<br>";
    const full_msg =
        "Thank you for your order! <br>" +
        req_msg +
        fname_msg +
        phone_msg +
        email_msg;
    const message = document.getElementById("message");
    //Adds the full_msg to the <p id="message"></p>
    message.innerHTML = full_msg;
    event.preventDefault();
});

// JS to not allow costumer to select dates from the past
let today = new Date();
let dd = today.getDate();
let mm = today.getMonth() + 1;
let yyyy = today.getFullYear();
if (dd < 10) {
    dd = "0" + dd;
}
if (mm < 10) {
    mm = "0" + mm;
}
today = yyyy + "-" + mm + "-" + dd;
document.getElementById("date").setAttribute("min", today);

// JS to get the current time when clicking the now button
document.getElementById("now_button").addEventListener("click", (event) => {
    event.preventDefault();
    const today = new Date();
    let timeNow = "";
    /* Due to how the getMinutes function returns the number of minutes, in the time space 00 to 09 adding a 0 is necessary to make the html work correctly */
    if (today.getMinutes() < 10) {
        timeNow = today.getHours() + ":" + "0" + today.getMinutes();
    } else {
        timeNow = today.getHours() + ":" + today.getMinutes();
    }
    const time_input = document.getElementById("time_input");
    console.log(today.getMinutes());
    time_input.setAttribute("value", timeNow);
});
