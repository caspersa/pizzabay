//Find which page is active
let path = window.location.pathname;
let page = path.split("/").pop();
let navbar_source = 'href="menu.html"';

//Assign class "active" if the page is active, which gives a green link in the navbar
let menu_class = "";
let booking_class = "";
let about_class = "";

if (page == "menu.html") {
    navbar_source = 'onclick="back_to_menu()"';
    menu_class = "active";
} else if (page == "booking.html") {
    booking_class = "active";
} else if (page == "about.html") {
    about_class = "active";
}

//Function to show and hide the hamburger menu
function hamburger_menu() {
    const x = document.getElementById("myLinks");
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
}

//Function to return to the home page
function back_to_front() {
    location.href = "index.html";
}

//Writes html for the navbar
document.write(
    '<div class="navbar_whole"><div class="flex_row_justifycenter" onclick="back_to_front()">\
    <img class="navbar_image" src="img/pizza_guy.png" alt"Pizza guy">\
    <div id="navbar_logo" class="flex_row_justifycenter" style="cursor: pointer">\
    <p class="green_text logo_text_shadow">PIZ</p>\
    <p class="white_text logo_text_shadow">ZA</p>\
    <p class="red_text logo_text_shadow">BAY</p>\
    </div>\
    <img class="navbar_image" src="img/pizza_guy.png" alt"Pizza guy">\
    </div>\
    <div id="hamburger_menu">\
    <div class="topnav">\
    <a href="index.html" class="active_hamburger">PIZZABAY</a>\
    <div id="myLinks">\
    <a href="index.html">FRONT PAGE</a>\
    <a href="menu.html">MENU & ORDER</a>\
    <a href="booking.html">RESERVATION</a>\
    <a href="about.html">ABOUT</a>\
    </div>\
    <a href="javascript:void(0);" class="icon" onclick="hamburger_menu()">\
    <i class="fa fa-bars"></i>\
    </a>\
    </div>\
    <button id="cart_hamburger_button" style="display: none;" onclick="go_to_cart()" class="button cart_button">CART</button>\
    </div>\
    <div id="navbar_link_bar">\
    <ul class="flex_row_center_center off_white_background no_list_style " id="link_list">\
    <span class="dot" style="background-color: green;"></span>\
    <li class="margin_30_left_right font_inika_18 navbar_text"><a class="links" href="index.html">FRONT PAGE</a></li>\
    <span class="dot" style="background-color: red;"></span>\
    <li class="margin_30_left_right font_inika_18 navbar_text"><a class="links ' +
        menu_class +
        '" id="menu_link"' +
        navbar_source +
        '>MENU & ORDER</a></li>\
    <span class="dot" style="background-color: green;"></span>\
    <li class="margin_30_left_right font_inika_18 navbar_text"><a class="links ' +
        booking_class +
        '" id="booking_link" href="booking.html">RESERVATION</a></li>\
    <span class="dot" style="background-color: red;"></span>\
    <li class="margin_30_left_right font_inika_18 navbar_text"><a class="links ' +
        about_class +
        '" id="about_link" href="about.html">ABOUT US</a></li>\
    <span class="dot" style="background-color: green;"></span>\
    <li class="margin_30_left_right"><button id="cart_navbar_button" style="display: none;" onclick="go_to_cart()" class="button cart_button">CART</button></li>\
    <span id="cart_dot" background-color: red;" class="dot"></span>\
    </ul>\
    </div>\
    </div>\
'
);
